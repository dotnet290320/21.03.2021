﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfDataContext
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Person
        {
            public string Name { get; set; }
            public int Age { get; set; }
            public override string ToString()
            {
                return $"Person name {Name} {Age}";
            }
        }

        public int Number { get; set; }

        public Person p { get; set; }
        public MainWindow()
        {
            Number = 36;
            InitializeComponent();

            p = new Person() { Name = DateTime.Now.ToString() };

            //mylbl.DataContext = p;
            mylbl.DataContext = this;
            mylbl2.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // p.Name = DateTime.Now.ToString();  // will not listen!

            p = new Person() { Name = DateTime.Now.ToString(), Age = p.Age + 10 };

            mylbl.DataContext = p;
        }
    }
}
